import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loggedUser: null
    },
    mutations: {
        addLoggedUser(state, loggedUser) {
            state.loggedUser = loggedUser;
        }
    }
});