import Vue from 'vue'
import Router from 'vue-router'
import login from './views/login.vue'
import home from './views/home.vue'
import SignUp from './views/SignUp.vue'
import store from './store';

Vue.use(Router)

function guard(to, from, next) {
  if (store.state.loggedUser) {
    next();
  } else {
    next('/login');
  }
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      component: login
    },
    {
      path: '/',
      component: home,
      beforeEnter: guard,
    },
    {
      path: '/sign-up',
      component: SignUp
    }
  ]
})
